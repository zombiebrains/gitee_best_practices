# Docker+Jenkins+Maven+码云自动构建部署springboot项目实践

## 前言

在分布式架构中，常需要更新和发布一个项目到多个服务器上，项目多的情况下，使用人工发布和维护比较麻烦，所以出现了使用Jenkins、Docker等工具实现自动构建发布项目的解决方案，在这里记录下我在实际开发过程中的重点操作步骤以及遇到的问题。

## 一、Docker安装

当前服务器为CentOS 7.4，安装最新docker-ce版本。

### 1.安装需要的软件包
    yum install -y yum-utils device-mapper-persistent-data lvm2

### 2.设置stable镜像仓库
    yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
    （这里建议使用的是阿里镜像）

### 3.更新yum软件包索引
    yum makecache fast

### 4.安装Docker CE
    yum -y install docker-ce

### 5.启动Docker
    systemctl start docker

### 6.测试
    docker version
    docker run hello-world
截止到这里，测试通过，docker就能正常使用

常用的Docker命令 
    #列出容器
    docker ps -a
    #杀掉容器进程
    docker kill jenkins
    #创建容器镜像
    docker run -it -m 2048M -p 10080:8080 -v jenkins-data:/var/jenkins_home --name jenkins jenkinsci/blueocean
    #进入容器
    docker exec -it jenkins bash
    #运行/停止/重启容器
    docker start/stop/restart jenkins

### 7.配置镜像加速（可选）
这里需要查看下自己服务器镜像加速地址
（贴个阿里云服务器的图）

![Image text](https://raw.githubusercontent.com/Function00/fun/master/jxjs.png)

添加图上红圈{"registry-mirrors": ["https://｛自已的编码｝.mirror.aliyuncs.com"]}

    mkdir -p /etc/docker
    vim  /etc/docker/daemon.json
    systemctl daemon-reload
    systemctl restart docker

## 二、Docker安装Jenkins

### 1.Docker运行Jenkins镜像容器
这边参考了Jenkins官方文档使用Docker安装Jenkins的方法，安装Jenkins/blueocean版本

![Image text](https://raw.githubusercontent.com/Function00/fun/master/jenkins_install.png)

    docker run \
    --rm \
    -u root \
    -p 8080:8080 \
    -v jenkins-data:/var/jenkins_home \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v "$HOME":/home \
    jenkinsci/blueocean
    
这边贴下我结合实际启动语句，设置2G运行内存，端口设置为10080，重命名为jenkins(挂载目录 -v jenkins-data  默认是指向服务器的 /var/lib/docker/volumes/jenkins_home，可以根据自己需要修改)
    docker run -it -m 2048M -p 10080:8080 -p 50000:50000 -v jenkins-data:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock -v "$HOME":/home --name jenkins jenkinsci/blueocean

第一次启动Jenkins要执行一些快速的 "一次性" 步骤,浏览器访问 http://localhost:10080 进行解锁Jenkins.
建议参考Jenkins官方文档 https://jenkins.io/zh/doc/tutorials/build-a-java-app-with-maven/

### 2.Jenkins准备工作
#### 系统管理--》插件管理

Maven Integration （可以直接添加Maven任务类型）

![Image text](https://raw.githubusercontent.com/Function00/fun/master/maven.png)

Publish Over SSH （项目构建完成可以ssh远程发送部署）

![Image text](https://raw.githubusercontent.com/Function00/fun/master/ssh.png)

#### 系统管理--》系统设置
配置ssh (需要有安装Publish Over SSH)

![Image text](https://raw.githubusercontent.com/Function00/fun/master/sshset.png)

name 服务名
Hostaname 主机地址
Username 登陆名
Passphrase 密码
Remote Directory 远程访问目录

#### 系统管理--》全局工具配置
配置项目对应jdk,可以自动安装，也可以手动配置jdk目录（容器目录，服务器上需要拷贝jdk到对应挂载目录，jenkins-data所在目录）,jenkins默认会使用自带的jdk1.8

![Image text](https://raw.githubusercontent.com/Function00/fun/master/jdk.png)

配置项目需要的maven版本

![Image text](https://raw.githubusercontent.com/Function00/fun/master/mavenc.png)

### 3.Jenkins创建任务
新建任务 可以选择"构建一个maven项目"(需要安装Maven Integration) 也可以 "构建一个自由风格软件项目"

![Image text](https://raw.githubusercontent.com/Function00/fun/master/sshset.png)

设置svn,码云上的springboot项目地址

![Image text](https://raw.githubusercontent.com/Function00/fun/master/config_svn.png)

构建触发器，这里设置定时执行，每30分钟自动检查SVN是否有更新，如有变动触发重新打包发布任务

![Image text](https://raw.githubusercontent.com/Function00/fun/master/hook.png)

控制台输出 和 maven选择
    clean install -DskipTests=true

![Image text](https://raw.githubusercontent.com/Function00/fun/master/config_maven.png)

构建完成后通过ssh发送项目，执行脚本命令

![Image text](https://raw.githubusercontent.com/Function00/fun/master/config_ssh.png)

可以直接填写命令
    #!/bin/sh
    echo "启动脚本"
    cd /usr/springboot/
    ./start.sh

start.sh 启动脚本
    #!/bin/sh
    echo "终止进程"
    p_id=`ps -ef | grep springboot-0.0.1-SNAPSHOT.jar | grep -v "grep" | awk '{print $2}'`
    echo $tomcat_id

    for id in $p_id
    do
        kill -9 $id
        echo "kill -9 $id"
    done

    sleep 10

    echo "进入目录"
    cd /usr/springboot/
    pwd
    echo "启动项目"
    nohup java -jar springboot-0.0.1-SNAPSHOT.jar >/dev/null 2>&1 &
    echo "完成"

到此，新建任务完成，进入到项目页面点击左侧立即构建进行测试，左下方会出现任务进程，点击进入进程可以查看控制台输出

![Image text](https://raw.githubusercontent.com/Function00/fun/master/project.png)
![Image text](https://raw.githubusercontent.com/Function00/fun/master/log.png)


## 三、进阶

### 1.主动触发构建

勾选触发远程构建,设置token
访问 JENKINS_URL/job/springboot/build?token=TOKEN_NAME 或者 /buildWithParameters?token=TOKEN_NAME 即可触发构建

![Image text](https://raw.githubusercontent.com/Function00/fun/master/config_hook.png)

1.主动访问触发构建，比如写成接口调用，这里不说明了。
2.提交代码，webhook触发，这里介绍下使用码云webhook实现，提交代码触发自动构建。

![Image text](https://raw.githubusercontent.com/Function00/fun/master/webHook.png)

    http://Username:API_TOKEN@Hostaname:10080/projectName/build?token=TOKEN形式

Usernam和API_TOKEN为Jenkins配置的用户登陆名及生成的API_TOKEN
可以在用户列表--》点击用户--》设置  创建一个API_TOKEN

![Image text](https://raw.githubusercontent.com/Function00/fun/master/apitoken.png)

也可以创建多个用户分配不同权限
系统管理--》全局安全设置

![Image text](https://raw.githubusercontent.com/Function00/fun/master/config_user.png)

### 2.发布war包到Docker的tomcat容器等

主要分为2个步骤
1.ssh传输war包到服务器。
2.拷贝war包到tomcat容器下。
    #安装tomcat
    docker pull docker.io/tomcat
    #创建tomcat容器
    docker run -p 8080:8080 docker.io/tomcat --name tomcat
    #拷贝war包到tomcat容器下    （这里的tomcat既上方 --name 的命名 也可以使用CONTAINER ID）
    docker cp springboot-SNAPSHOT.war tomcat:/usr/local/tomcat/webapps
    #重启tomcat
    docker restart tomcat

如果创建tomcat容器时挂载了目录
    docker run -p 8080:8080 docker.io/tomcat -v /usr/webapps:/usr/local/tomcat/webapps --name tomcat
其实就只需要一步完成，直接ssh传输到挂载目录/usr/webapps即可 （重启tomcat）

同理发布为Docker镜像，先建立Docker私有仓库，将war包、jar包build到仓库，再使用docker pull 命令安装，使用过Maven本地仓库的同学肯定非常眼熟，这里不细写了。

## 遇到过的问题

### 1.403
调用webhook返回403等，jenkins是REST_api风格，默认调用接口必须有权限。
或者尝试系统管理--》全局安全设置  
1.授权策略 给匿名用户授权 参考进阶的用户分配权限图
2.跨站请求伪造保护 取消勾选

### 2.没有“构建一个maven项目”选项
系统管理--》插件管理 安装插件 Maven Integration

### 3.jdk、maven No such file or directory
进docker的jenkins目录 /var/jenkins_home/ 检查下自动安装的jdk或maven有没有下载成功。
设置多个jkd或maven,项目设置里切换jdk、maven版本，或使用system默认jdk。
检查maven的pom文件，是否有jar包是私人仓库（-_-）。


